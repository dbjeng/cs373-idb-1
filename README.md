# cs373-idb-1

### Group Members

<!-- prettier-ignore -->
| Name             | GitLabID           | EID     |
| ---------------  | ------------------ | ------- |
| Brady Wilkin     | @bradywilkin       | bdw2457 |
| Jerome Conkright | @jerome.conkright1 | jbc3467 |
| Daniel Jeng      | @dbjeng            | Dbj495  |
| Aaron Lee        | @aaronlee232       | anl2947 |
| Harman Sandhu    | @hasandhu          | hjs2245 |

### Git SHA

<!-- prettier-ignore -->
| Phase | Git Sha                                  |
| ----- | ---------------------------------------- |
| 1     | cdfff33f8aa8e442a31c19cc4634bc7a04a013f1 |
| 2     | 28cd145db3878273cfc448773dea116f34bdf4a1 |
| 3     | 67fdbb36c4cf21b7c2304e1499afd63da52fc724 |
| 4     | 5b4accd184026edbbfb1093487208584fd2d356c |

### Project Leader

<!-- prettier-ignore -->
| Phase | Project Leader   |
| ----- | ---------------- |
| 1     | Aaron Lee        |
| 2     | Jerome Conkright |
| 3     | Harman Sandhu    |
| 4     | Daniel Jeng      |

#### Responsibilities

Organize and direct group meetings, make sure everyone is on track

# GitLab Pipelines

https://gitlab.com/dbjeng/cs373-idb-1/-/pipelines

# Website Link

https://www.allthesports.info/

# Completion Times

### Phase 1

<!-- prettier-ignore -->
| Name             | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ---------------- | -------------------------- | -------------------------- |
| Brady Wilkin     | 12                         | 15                         |
| Jerome Conkright | 13                         | 11                         |
| Daniel Jeng      | 11                         | 10                         |
| Aaron Lee        | 15                         | 22                         |
| Harman Sandhu    | 10                         | 13                         |

### Phase 2

<!-- prettier-ignore -->
| Name             | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ---------------- | -------------------------- | -------------------------- |
| Brady Wilkin     | 14                         | 23                         |
| Jerome Conkright | 10                         | 15                         |
| Daniel Jeng      | 15                         | 45                         |
| Aaron Lee        | 15                         | 45                         |
| Harman Sandhu    | 14                         | 39                         |

### Phase 3

<!-- prettier-ignore -->
| Name             | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ---------------- | -------------------------- | -------------------------- |
| Brady Wilkin     | 15                         | 12                         |
| Jerome Conkright | 15                         | 20                         |
| Daniel Jeng      | 12                         | 24                         |
| Aaron Lee        | 13                         | 15                         |
| Harman Sandhu    | 15                         | 20                         |

<!-- ### Phase 4 -->

<!-- prettier-ignore -->
| Name             | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| ---------------- | -------------------------- | -------------------------- |
| Brady Wilkin     | 5                          | 5                          |
| Jerome Conkright | 6                          | 10                         |
| Daniel Jeng      | 7                          | 5                          |
| Aaron Lee        | 4                          | 5                          |
| Harman Sandhu    | 10                         | 6                          |

### Phase EX

| Name      | Est. Completion Time (hrs) | Real Completion Time (hrs) |
| --------- | -------------------------- | -------------------------- |
| Aaron Lee | 30                         | 34                         |

#### AWS Cost Optimizations:

- Switched from RDS to self-hosted EC2 MySQL server
- Switched from elastic beanstalk to lightsail instance with self-configured container and SSL support

#### Adjustments to Project Structure

- docker-compose.yml purpose switched establishing local dev environment to establishing production containers in lightsail instance
- Makefile alterred to include development and production commands
- Organized Flask code and directories
- Revamped CI/CD pipeline to test and deploy, not just build and test
