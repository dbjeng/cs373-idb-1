.DEFAULT_GOAL := full-dev

# ==================================================================
# Development Environment Commands
# ==================================================================

full-dev: backend-dev frontend-dev

frontend-dev:
	( cd frontend && npm run dev )

backend-dev:
	( cd backend && flask run )

# ==================================================================
# Production Environment Commands
# ==================================================================

backend-build: 
	docker-compose build

backend-run:
	docker-compose up -d

# ==================================================================
# Utility Commands
# ==================================================================

clean:
	docker system prune

clean-all:
	docker system prune --all --volumes

redeploy:
	touch redeploy && echo "Unique Text: $$(uuidgen)" > redeploy && git add redeploy && git commit -m "testing redeployment" && git push && rm redeploy

install-frontend-dep:
	npm ci --prefix ./frontend

install-backend-dep:
	(cd backend && pip install -r requirements.txt)

# ==================================================================
# Task Commands
# ==================================================================

backend-format:
	docker-compose up --build -d backend
	docker-compose exec backend sh -c "black ."
	docker-compose down

backend-tests:
	python3 backend/tests.py

frontend-tests:
	(cd frontend && npm run test)

# ==================================================================
# Production Debug Commands
# ==================================================================

flask-shell:
	docker exec -it backend /bin/sh

nginx-shell:
	docker exec -it nginx /bin/sh
