import aaronpic from './aaronpic.jpg'
import harmanpic from './harmanpic.jpg'
import harmanpic2 from './harmanpic2.jpg'
import bradypic from './bradypic.png'
import jeromepic from './jeromepic.jpg'
import danielpic from './danielpic.jpg'
import postman from './postman.png'
import gitlab from './gitlab.png'
import thesportsdb from './sportsdb.png'
import sportsscore from './sportscore.png'
import youtube from './youtube.png'
import googlemaps from './googlemaps.png'
import genleg from './genericleague.png'
import genteam from './genericteam.png'
import anything from './allthesports logo.png'




export{
    aaronpic,
    harmanpic,
    harmanpic2,
    bradypic,
    jeromepic,
    danielpic,
    postman,
    gitlab,
    thesportsdb,
    sportsscore,
    youtube,
    googlemaps,
    genleg,
    genteam,
    anything
}