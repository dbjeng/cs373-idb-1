module.exports = async () => {
    return {
      verbose: true,
      testEnvironment: 'jsdom',
      testMatch: [
        '**/*.test.(js|cjs)'
      ],
      "moduleNameMapper": {
        "\\.(jpg|jpeg|png|gif|svg)$": "<rootDir>/__mocks__/fileMock.js"
      },
      "transform": {
        "^.+\\.jsx?$": "babel-jest",
        "^.+\\.css$": "<rootDir>/node_modules/jest-transform-stub"
      }
    };
  };
  