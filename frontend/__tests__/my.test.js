import React from 'react';
import { render, screen, fireEvent, cleanup, waitFor } from '@testing-library/react';
// import axios from 'axios';
import Players from '../src/scenes/Players';
import Leagues from '../src/scenes/Leagues';
import Teams from '../src/scenes/Teams';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
import Home from '../src/scenes/Home';
import About from '../src/scenes/About';


afterEach(() => {
  cleanup();
});

test('Home component renders without errors', async () => {
  render(<BrowserRouter><Home /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText(/Get the ball rolling.../)).toBeInTheDocument();
  });
});

test('About renders without errors', async () => {
  render(<BrowserRouter><About /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText(/Meet the allthesports team/)).toBeInTheDocument();
  });
});

// Test that the Leagues component renders without errors
test('Leagues component renders without errors', async () => {
  render(<BrowserRouter><Leagues /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText('Leagues')).toBeInTheDocument();
  });
});

// Test that the Teams component renders without errors
test('Teams component renders without errors', async () => {
  render(<BrowserRouter><Teams /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText((content, element) => {
      return element.tagName.toLowerCase() === 'h1' && content.includes('Teams');
    })).toBeInTheDocument();
  });
});

// Test that the Players component renders without errors
test('Players component renders without errors', async () => {
  render(<BrowserRouter><Players /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText('Players')).toBeInTheDocument();
  });
});

test('Clicking on a navbar link navigates to the corresponding page', async () => {
  render(<BrowserRouter><Home /></BrowserRouter>);
  const link = screen.getByText('Leagues');
  fireEvent.click(link);
  await waitFor(() => {
    expect(screen.getByText('Leagues')).toBeInTheDocument();
  });
});

test('Clicking on a navbar link navigates to the corresponding page 2', async () => {
  render(<BrowserRouter><Home /></BrowserRouter>);
  const link = screen.getByText('Teams');
  fireEvent.click(link);
  await waitFor(() => {
    expect(screen.getByText('Teams')).toBeInTheDocument();
  });
});

test('Clicking on a navbar link navigates to the corresponding page 3', async () => {
  render(<BrowserRouter><Home /></BrowserRouter>);
  const link = screen.getByText('Players');
  fireEvent.click(link);
  await waitFor(() => {
    expect(screen.getByText('Players')).toBeInTheDocument();
  });
});

test('About has name', async () => {
  render(<BrowserRouter><About /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText('Brady Wilkin')).toBeInTheDocument();
  });
});

test('About has tools', async () => {
  render(<BrowserRouter><About /></BrowserRouter>);
  await waitFor(() => {
    expect(screen.getByText('Tools')).toBeInTheDocument();
  });
});