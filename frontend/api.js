const getLeagues = async () => {
    const response = await fetch('https://api.allthesports.info/leagues');
    const data = await response.json();
    return data;
  };
  
  export { getLeagues };