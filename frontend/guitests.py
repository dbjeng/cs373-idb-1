# inspired by NutriNet https://gitlab.com/goesoscar/NutriNet/
import unittest
import time
import os
from selenium import webdriver
from selenium import common
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service

url = "https://www.allthesports.info/"

class GuiTests(unittest.TestCase):
    # chromedriver_path = os.path.join(os.getcwd(), 'node_modules', '.bin', 'chromedriver')
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--window-size=1920,1080")
        self.driver = webdriver.Chrome(options=chrome_options)

    def tearDown(self):
        self.driver.quit()

    def test1(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='About']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "about")
    
    def test2(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Leagues']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "leagues")

    def test3(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Teams']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "teams")

    def test4(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Players']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "players")

    def test5(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Visualizations']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "visualizations")

    def test6(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Home']")
        button.click()
        self.assertEqual(self.driver.current_url, url + "")

    def test7(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='About']")
        button.click()
        button = self.driver.find_element(By.XPATH, "//a[normalize-space()='Home']")
        button.click()
        self.assertEqual(self.driver.current_url, url)

    def test8(self):
        self.driver.get(url)
        search_bar = self.driver.find_element(By.XPATH, "//input[@class='form-control']")
        search_bar.send_keys("james")
        search_bar.submit()
        WebDriverWait(self.driver, 10).until(EC.url_contains("search?query=james"))
        teams = self.driver.find_element(By.XPATH, "//button[normalize-space()='Teams']")
        teams.click()
        time.sleep(3)
        search_results_div = self.driver.find_element(By.XPATH, "//div[@class='fade tab-pane active show']")
        search_results = search_results_div.find_elements(By.XPATH, "//div[@class='card']")
        self.assertGreater(len(search_results), 0)

    def test9(self):
        self.driver.get(url)
        search_bar = self.driver.find_element(By.XPATH, "//input[@class='form-control']")
        search_bar.send_keys("james")
        search_bar.submit()
        WebDriverWait(self.driver, 10).until(EC.url_contains("search?query=james"))
        players = self.driver.find_element(By.XPATH, "//button[normalize-space()='Players']")
        players.click()
        time.sleep(3)
        search_results_div = self.driver.find_element(By.XPATH, "//div[@class='fade tab-pane active show']")
        search_results = search_results_div.find_elements(By.XPATH, "//div[@class='card']")
        self.assertGreater(len(search_results), 0)

    def test10(self):
        self.driver.get(url + "leagues")
        dropdown_button = self.driver.find_element(By.XPATH, "//button[text()='Country']")
        dropdown_button.click()
        australia_option = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//a[text()='Australia']")))
        assert australia_option.is_displayed()




if __name__ == "__main__":
    unittest.main()
