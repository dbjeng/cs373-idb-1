from app import app
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from .models import Sport, League, Fact, Season, Team, Metric, Player

# Init marshmallow
ma = Marshmallow(app)


# Define Schemas
class SportSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Sport


class LeagueSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = League


class FactSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Fact


class SeasonSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Season


class TeamSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Team


class MetricSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Metric


class PlayerSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Player


# Initialize Schemas
sport_schema = SportSchema()
sports_schema = SportSchema(many=True)

league_schema = LeagueSchema()
leagues_schema = LeagueSchema(many=True)

fact_schema = FactSchema()
facts_schema = FactSchema(many=True)

season_schema = SeasonSchema()
seasons_schema = SeasonSchema(many=True)

team_schema = TeamSchema()
teams_schema = TeamSchema(many=True)

metric_schema = MetricSchema()
metrics_schema = MetricSchema(many=True)

player_schema = PlayerSchema()
players_schema = PlayerSchema(many=True)
