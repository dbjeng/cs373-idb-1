from app import app
from flask import request, jsonify, Response
import json
from sqlalchemy import or_
from sqlalchemy import and_
from sqlalchemy.orm import joinedload
from sqlalchemy import not_
from .models import db, Sport, League, Fact, Season, Team, Metric, Player
from .schema import *
from .populate import populate_db

@app.route('/')
def hello_world():
   return "Server is running!"

# Get 54 leagues
@app.route("/leagues", methods=["GET"])
def get_leagues():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    leagues = db.session.query(League)
    # leagues = leagues.order_by(League.map_src == None, League.map_src.desc())
    if page is not None:
        leagues = paginate(leagues, page, perPage)
    result = leagues_schema.dump(leagues, many=True)
    for i, league in enumerate(leagues):
        result[i]["sport_id"] = league.sport_id
    return result


@app.route("/total_leagues", methods=["GET"])
def get_total_leagues():
    count = db.session.query(League).count()
    return jsonify(count)


# Get Teams
@app.route("/teams", methods=["GET"])
def get_teams():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    teams = db.session.query(Team)
    if page is not None:
        teams = paginate(teams, page, perPage)
    result = teams_schema.dump(teams, many=True)
    for i, team in enumerate(teams):
        result[i].update(
            {
                "league_id": team.league_id,
                "sport_id": team.sport_id,
                "season_id": team.season_id,
            }
        )
    return result


@app.route("/total_teams", methods=["GET"])
def get_total_teams():
    count = db.session.query(Team).count()
    return jsonify(count)


# Get Players
@app.route("/players", methods=["GET"])
def get_players():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    players = db.session.query(Player)
    if page is not None:
        players = paginate(players, page, perPage)
    result = players_schema.dump(players, many=True)
    for i, player in enumerate(players):
        result[i].update(
            {
                "team_id": player.team_id,
                "league_id": player.league_id,
                "sport_id": player.sport_id,
            }
        )
    return result


@app.route("/total_players", methods=["GET"])
def get_total_players():
    count = db.session.query(Player).count()
    return jsonify(count)


# Get Sports
@app.route("/sports", methods=["GET"])
def get_sports():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    sports = db.session.query(Sport)
    if page is not None:
        sports = paginate(sports, page, perPage)
    result = sports_schema.dump(sports, many=True)
    return result


# Get League by ID
@app.route("/leagues/<int:id>", methods=["GET"])
def get_league(id):
    league = League.query.get(id)
    league_json = league_schema.dump(league)
    league_json["sport_id"] = league.sport_id
    if not league_json:
        return return_error("League not found")
    return league_json


# Get Team by ID
@app.route("/teams/<int:id>", methods=["GET"])
def get_team(id):
    team = Team.query.get(id)
    team_json = team_schema.dump(team)
    team_json["league_id"] = team.league_id
    team_json["sport_id"] = team.sport_id
    if not team_json:
        return return_error("Team not found")
    return team_json


# Get Player by ID
@app.route("/players/<int:id>", methods=["GET"])
def get_player(id):
    player = Player.query.get(id)
    player_json = player_schema.dump(player)
    player_json["league_id"] = player.league_id
    player_json["team_id"] = player.team_id
    player_json["sport_id"] = player.sport_id
    if not player_json:
        return return_error("Player not found")
    return player_json


# Get Sport by ID
@app.route("/sports/<int:id>", methods=["GET"])
def get_sport(id):
    sport = Sport.query.get(id)
    sport_json = sport_schema.dump(sport)
    if not sport_json:
        return return_error("Sport not found")
    return sport_json


# Get all facts of given league_id
@app.route("/facts/<int:id>", methods=["GET"])
def get_facts(id):
    facts = Fact.query.filter_by(league_id=id)
    facts_json = facts_schema.dump(facts, many=True)
    if not facts_json:
        return return_error("Facts not found")
    return facts_json


# Get all Seasons of given league_id
@app.route("/seasons/<int:id>", methods=["GET"])
def get_seasons(id):
    seasons = Season.query.filter_by(league_id=id)
    seasons_json = seasons_schema.dump(seasons, many=True)
    if not seasons_json:
        return return_error("Seasons not found")
    return seasons_json


# Get al metrics of given team_id
@app.route("/metrics/<int:id>", methods=["GET"])
def get_metrics(id):
    metrics = Metric.query.filter_by(team_id=id)
    metrics_json = metrics_schema.dump(metrics, many=True)
    if not metrics_json:
        return return_error("Metrics not found")
    return metrics_json


# Get Teams by League ID
@app.route("/teams/league/<int:id>", methods=["GET"])
def get_teams_by_league(id):
    teams = Team.query.filter_by(league_id=id)
    teams_json = teams_schema.dump(teams, many=True)
    if not teams_json:
        return return_error("Teams not found")
    return teams_json


# Get Players by Team ID
@app.route("/players/team/<int:id>", methods=["GET"])
def get_players_by_team(id):
    players = Player.query.filter_by(team_id=id)
    players_json = players_schema.dump(players, many=True)
    if not players_json:
        return return_error("Players not found")
    return players_json


# Get Search Results
@app.route("/search", methods=["GET"])
def search_all():
    query = request.args.get("query")
    keywords = query.split()
    occurances = {
        **search_leagues(keywords),
        **search_teams(keywords),
        **search_players(keywords),
    }

    # Need to sort by occurances to get most relevant search results in front

    # return occurances
    return occurances


@app.route("/testing", methods=["GET"])
def get_league_sport():
    league = League.query.get(1)
    return sport_schema.dump(league.sport)


@app.route("/join", methods=["GET"])
def testing_join():
    result = db.session.query(League).options(joinedload(League.sport)).all()
    return leagues_schema.dump(result)


############################################
# LEAGUES #
############################################


@app.route("/leagues/search", methods=["GET"])
def search_leagues_endpoint():
    keywords = request.args.get("terms")
    keywords_list = keywords.split(",")
    results = search_leagues(keywords_list)["leagues"]
    return jsonify(results)


# Instance Search Methods
def search_leagues(keywords):
    # occurances = {} # Count number of occurances of keyword in each instance. highest number = most relevant
    for keyword in keywords:
        queries = []
        queries.append(League.name.contains(keyword))
        queries.append(League.country.contains(keyword))
        queries.append(League.logo.contains(keyword))
        queries.append(League.start_date.contains(keyword))
        queries.append(League.end_date.contains(keyword))
        queries.append(League.map_src.contains(keyword))
        queries.append(League.sport.has(Sport.name.contains(keyword)))

    # Find all league instances that match search query
    leagues = League.query.filter(or_(*queries))
    json_leagues = leagues_schema.dump(leagues)

    for i in range(len(json_leagues)):
        json_leagues[i]["sport_id"] = leagues[i].sport_id

    # Count number of "occurances" or search queries that each league passed
    # Measures relevancy of each league instance

    return {"leagues": json_leagues}


# Sort for Leagues
# Input would look like this: /sort/leagues?sort_type=age
@app.route("/leagues/sort")
def sort_leagues():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    sort_type = request.args.get("sort_type")
    leagues = []

    if sort_type == "Name":
        leagues = League.query.filter(League.name != None).order_by(League.name)
    elif sort_type == "emaN":
        leagues = League.query.filter(League.name != None).order_by(League.name.desc())
    elif sort_type == "Country":
        leagues = League.query.filter(
            League.country != "No information about host coun"
        ).order_by(League.country)
    elif sort_type == "yrtnuorC":
        leagues = League.query.filter(
            League.country != "No information about host coun"
        ).order_by(League.country.desc())
    elif sort_type == "Start Date":
        leagues = League.query.filter(
            League.start_date != "No start date available"
        ).order_by(League.start_date)
        # Convert the start dates to datetime objects for sorting down to the minutes
        # leagues = sorted(leagues, key=lambda x: datetime.strptime(x.start_date, "%Y-%m-%d %H:%M:%S"))
    elif sort_type == "teD tnatS":
        leagues = League.query.filter(
            League.start_date != "No start date available"
        ).order_by(League.start_date.desc())
        # Convert the start dates to datetime objects for sorting down to the minutes
        # leagues = sorted(leagues, key=lambda x: datetime.strptime(x.start_date, "%Y-%m-%d %H:%M:%S"), reverse=True)
    elif sort_type == "End Date":
        leagues = League.query.filter(
            League.end_date != "No end date available"
        ).order_by(League.end_date)
        # Convert the end dates to datetime objects for sorting down to the minutes
        # leagues = sorted(leagues, key=lambda x: datetime.strptime(x.end_date, "%Y-%m-%d %H:%M:%S"))
    elif sort_type == "teD dnE":
        leagues = League.query.filter(
            League.end_date != "No end date available"
        ).order_by(League.end_date.desc())
        # Convert the end dates to datetime objects for sorting down to the minutes
        # leagues = sorted(leagues, key=lambda x: datetime.strptime(x.end_date, "%Y-%m-%d %H:%M:%S"), reverse=True)
    elif sort_type == "Sport":
        leagues = (
            League.query.join(Sport, onclause=League.sport_id == Sport.id)
            .filter(Sport.name != None)
            .order_by(Sport.name)
        )
    if page is not None:
        leagues = paginate(leagues, page, perPage)
    result = leagues_schema.dump(leagues, many=True)
    for i, leagues in enumerate(leagues):
        result[i].update({"sport_id": leagues.sport_id})
    return result


# Filter for Leagues
# Input would look like this: /filter/leagues?filter_type=age&filter_value=20
@app.route("/leagues/filter")
def filter_leagues():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    filter_type = request.args.get("filter_type")
    filter_value = request.args.get("filter_value")
    leagues = db.session.query(League)

    if filter_type == "Name":
        leagues = League.query.filter(League.name == filter_value)
    elif filter_type == "Country":
        leagues = League.query.filter(League.country == filter_value)
    elif filter_type == "Sport":
        leagues = []
        sport_id = (
            db.session.query(Sport.id).filter(Sport.name == filter_value).scalar()
        )
        leagues = League.query.filter(League.sport_id == sport_id)
    elif filter_type == "Start Date":
        if filter_value == "2023":
            leagues = League.query.filter(League.start_date.like("2023%"))
        elif filter_value == "2022":
            leagues = League.query.filter(League.start_date.like("2022%"))
        else:
            leagues = League.query.filter(
                ~League.start_date.like("2023%"),
                ~League.start_date.like("2022%"),
                League.start_date.isnot(None),
                League.start_date != "No start date available",
            )
    elif filter_type == "End Date":
        if filter_value == "2023":
            leagues = League.query.filter(League.end_date.like("2023%"))
        elif filter_value == "2022":
            leagues = League.query.filter(League.end_date.like("2022%"))
        else:
            leagues = League.query.filter(
                ~League.end_date.like("2023%"),
                ~League.end_date.like("2022%"),
                League.end_date.isnot(None),
                League.end_date != "No end date available",
            )
    elif filter_type == "map":
        # If filter_value is "yes", then we want to return all leagues that have a map
        if filter_value == "Yes":
            leagues = League.query.filter(League.map_src != "")
        # If filter_value is "no", then we want to return all leagues that do not have a map
        elif filter_value == "No":
            leagues = League.query.filter(League.map_src == "")

    if page is not None:
        leagues = paginate(leagues, page, perPage)

    result = leagues_schema.dump(leagues, many=True)
    for i, league in enumerate(leagues):
        result[i].update({"sport_id": league.sport_id})

    return result


############################################
# Teams #
############################################


@app.route("/teams/search", methods=["GET"])
def search_teams_endpoint():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    keywords_list = str(request.args.get("terms")).split(" ")
    teams = search_teams(keywords_list, page, perPage)["teams"]
    return jsonify(teams)


def search_teams(keywords, page=None, perPage=None):
    for keyword in keywords:
        queries = []
        queries.append(Team.name.contains(keyword))
        queries.append(Team.stadium_name.contains(keyword))
        queries.append(Team.stadium_capacity.contains(keyword))
        queries.append(Team.country_name.contains(keyword))
        queries.append(Team.manager_name.contains(keyword))
        queries.append(Team.manager_birthday.contains(keyword))
        queries.append(Team.sport.has(Sport.name.contains(keyword)))
        queries.append(Team.league.has(League.name.contains(keyword)))
        queries.append(Team.season.has(Season.name.contains(keyword)))
    teams = Team.query.filter(or_(*queries))
    result = team_schema.dump(teams, many=True)
    for i, team in enumerate(teams):
        result[i].update(
            {
                "season_id": team.season_id,
                "league_id": team.league_id,
                "sport_id": team.sport_id,
            }
        )
    if page is not None:
        teams = paginate(teams, page, perPage)
    return {"teams": result}


@app.route("/teams/filter")
def filter_teams():
    filter_type = request.args.get("filter_type")
    filter_value = request.args.get("filter_value")
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    teams = []

    if filter_type == "sport":
        sport_id = db.session.query(Sport.id).filter(Sport.name == filter_value)
        teams = db.session.query(Team).filter(Team.sport_id == sport_id)
    elif filter_type == "country":
        teams = Team.query.filter(Team.country_name == filter_value)
    elif filter_type == "league":
        league_id = db.session.query(League.id).filter(League.name == filter_value)
        teams = db.session.query(Team).filter(Team.league_id == league_id)
    elif filter_type == "stadium_capacity":
        filter_value = filter_value.split("-")
        teams = Team.query.filter(
            and_(
                int(filter_value[0]) < Team.stadium_capacity,
                Team.stadium_capacity < int(filter_value[1]),
            )
        )
    elif filter_type == "has_logo":
        teams = Team.query.filter(
            Team.logo != "https://tipsscore.com/resb/no-league.png"
            if filter_value == "Yes"
            else Team.logo == "https://tipsscore.com/resb/no-league.png"
        )

    if page is not None:
        teams = paginate(teams, page, perPage)
    result = teams_schema.dump(teams, many=True)
    for i, team in enumerate(teams):
        result[i].update({"league_id": team.league_id, "sport_id": team.sport_id})
    return result


@app.route("/teams/sort")
def sort_teams():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    sort_type = request.args.get("sort_type")
    teams = []

    if sort_type == "Name":
        teams = Team.query.order_by(Team.name)
    elif sort_type == "Stadium Capacity":
        teams = Team.query.order_by(Team.stadium_capacity).filter(
            Team.stadium_capacity != 0
        )
    elif sort_type == "Country Name":
        teams = Team.query.order_by(Team.country_name).filter(Team.country_name != None)
    elif sort_type == "Stadium Name":
        teams = Team.query.order_by(Team.stadium_name).filter(
            Team.stadium_name != "No information about stadium."
        )
    elif sort_type == "Manager Name":
        teams = Team.query.order_by(Team.manager_name).filter(
            Team.manager_name != "No information about manager n"
        )

    if page is not None:
        teams = paginate(teams, page, perPage)
    result = teams_schema.dump(teams, many=True)
    for i, team in enumerate(teams):
        result[i].update({"league_id": team.league_id, "sport_id": team.sport_id})
    return result


############################################
# Players #
############################################


# Player search endpoint
# Input would look like this: /players/search?terms=blues&terms=predators
@app.route("/players/search", methods=["GET"])
def search_players_endpoint():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    keywords_list = str(request.args.get("terms")).split(" ")
    players = search_players(keywords_list, page, perPage)["players"]
    return jsonify(players)


# Search for players
def search_players(keywords, page=None, perPage=None):
    queries = []
    for keyword in keywords:
        queries.append(Player.name.contains(keyword))
        queries.append(Player.weight.contains(keyword))
        queries.append(Player.age.contains(keyword))
        queries.append(Player.height.contains(keyword))
        queries.append(Player.birthday.contains(keyword))
        queries.append(Player.market_currency.contains(keyword))
        queries.append(Player.market_value.contains(keyword))
        queries.append(Player.rating.contains(keyword))
        queries.append(Player.bio.contains(keyword))
        queries.append(Player.sport.has(Sport.name.contains(keyword)))
        queries.append(Player.league.has(League.name.contains(keyword)))
        queries.append(Player.team.has(Team.name.contains(keyword)))
    players = Player.query.join(Team, onclause=Player.team_id == Team.id).filter(
        or_(*queries)
    )
    result = players_schema.dump(players, many=True)
    for i, player in enumerate(players):
        result[i].update(
            {
                "team_id": player.team_id,
                "league_id": player.league_id,
                "sport_id": player.sport_id,
            }
        )
    if page is not None:
        players = paginate(players, page, perPage)
    return {"players": result}


# Filter for players
# Input would look like this: /filter/players?filter_type=sport&filter_value=Football
@app.route("/players/filter")
def filter_players():
    filter_type = request.args.get("filter_type")
    filter_value = request.args.get("filter_value")
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    players = []

    if filter_type == "sport":
        sport_id = db.session.query(Sport.id).filter(Sport.name == filter_value)
        players = db.session.query(Player).filter(Player.sport_id == sport_id)
    elif filter_type == "country":
        players = Player.query.join(Team).filter(Team.country_name == filter_value)
    elif filter_type == "photo":
        players = Player.query.filter(
            Player.photo != "https://tipsscore.com/resb/no-photo.png"
            if filter_value == "Yes"
            else Player.photo == "https://tipsscore.com/resb/no-photo.png"
        )
    elif filter_type == "biography":
        players = Player.query.filter(
            Player.bio != "" if filter_value == "Yes" else Player.bio == ""
        )
    elif filter_type == "video":
        players = Player.query.filter(
            Player.video_src != "" if filter_value == "Yes" else Player.video_src == ""
        )

    if page is not None:
        players = paginate(players, page, perPage)
    result = players_schema.dump(players, many=True)
    for i, player in enumerate(players):
        result[i].update(
            {
                "team_id": player.team_id,
                "league_id": player.league_id,
                "sport_id": player.sport_id,
            }
        )

    return result


# Sort for players
# Input would look like this: /sort/players?sort_type=age
@app.route("/players/sort")
def sort_players():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    sort_type = request.args.get("sort_type")
    players = []

    if sort_type == "Age":
        players = Player.query.order_by(Player.age).filter(Player.age)
    elif sort_type == "Weight":
        players = Player.query.order_by(Player.weight).filter(Player.weight)
    elif sort_type == "Height":
        players = Player.query.order_by(Player.height).filter(Player.height)
    elif sort_type == "Name":
        players = Player.query.order_by(Player.name)
    elif sort_type == "Team Name":
        players = Player.query.join(Team, onclause=Player.team_id == Team.id).order_by(
            Team.name
        )

    if page is not None:
        players = paginate(players, page, perPage)
    result = players_schema.dump(players, many=True)
    for i, player in enumerate(players):
        result[i].update(
            {
                "team_id": player.team_id,
                "league_id": player.league_id,
                "sport_id": player.sport_id,
            }
        )
    return result

def return_error(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="app/json")
    resp.error_code = 404
    return resp

def paginate(query, page_n, n_per_page=20):
    return query.paginate(page=page_n, per_page=n_per_page, error_out=False).items
