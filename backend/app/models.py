from app import app
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# Init db
db = SQLAlchemy(app)


class Sport(db.Model):
    __tablename__ = "sports"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    leagues = db.relationship("League", back_populates="sport")
    teams = db.relationship("Team", back_populates="sport")
    players = db.relationship("Player", back_populates="sport")

    def __init__(self, id, name):
        self.id = id
        self.name = name


class League(db.Model):
    __tablename__ = "leagues"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    country = db.Column(db.String(30))
    logo = db.Column(db.String(200))
    start_date = db.Column(db.String(30))
    end_date = db.Column(db.String(30))
    map_src = db.Column(db.String(200))
    sport_id = db.Column(db.Integer, db.ForeignKey("sports.id"))
    players = db.relationship("Player", back_populates="league")

    # Note on ForeignKeys: ForeignKeys create relationships between tables, while back_populates
    # makes sure that they are updated dynamically. The ForeignKey can be thought of as a pointer.
    # Note on relationships: They basically create an attribute in the class they are related to.
    # The below relationship makes a 'league' attribute for the Fact class
    facts = db.relationship("Fact", back_populates="league")
    seasons = db.relationship("Season", back_populates="league")
    teams = db.relationship("Team", back_populates="league")
    sport = db.relationship("Sport", back_populates="leagues")

    def __init__(self, id, name, country, logo, start_date, end_date, map_src, sport):
        self.id = id
        self.name = name
        self.country = country
        self.logo = logo
        self.start_date = start_date
        self.end_date = end_date
        self.map_src = map_src
        self.sport = sport


class Fact(db.Model):
    __tablename__ = "facts"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    desc = db.Column(db.String(60))
    league_id = db.Column(db.Integer, db.ForeignKey("leagues.id"))
    league = db.relationship("League", back_populates="facts")

    def __init__(self, name, desc, league):
        self.name = name
        self.desc = desc
        self.league = league


class Season(db.Model):
    __tablename__ = "seasons"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    start_year = db.Column(db.Integer)
    end_year = db.Column(db.Integer)
    league_id = db.Column(db.Integer, db.ForeignKey("leagues.id"))
    league = db.relationship("League", back_populates="seasons")
    teams = db.relationship("Team", back_populates="season")

    def __init__(self, id, name, start_year, end_year, league):
        self.id = id
        self.name = name
        self.start_year = start_year
        self.end_year = end_year
        self.league = league


class Team(db.Model):
    __tablename__ = "teams"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    stadium_name = db.Column(db.String(60))
    stadium_capacity = db.Column(db.Integer)
    country_name = db.Column(db.String(30))
    manager_name = db.Column(db.String(30))
    manager_photo = db.Column(db.String(200))
    manager_birthday = db.Column(db.String(30))
    map_src = db.Column(db.String(200))
    logo = db.Column(db.String(200))
    sport_id = db.Column(db.Integer, db.ForeignKey("sports.id"))
    league_id = db.Column(db.Integer, db.ForeignKey("leagues.id"))
    season_id = db.Column(db.Integer, db.ForeignKey("seasons.id"))

    sport = db.relationship("Sport", back_populates="teams")
    league = db.relationship("League", back_populates="teams")
    season = db.relationship("Season", back_populates="teams")
    metrics = db.relationship("Metric", back_populates="team")
    players = db.relationship("Player", back_populates="team")

    def __init__(
        self,
        id,
        name,
        stadium_name,
        stadium_capacity,
        country_name,
        manager_name,
        manager_photo,
        manager_birthday,
        map_src,
        logo,
        sport,
        league,
        season,
    ):
        self.id = id
        self.name = name
        self.stadium_name = stadium_name
        self.stadium_capacity = stadium_capacity
        self.country_name = country_name
        self.manager_name = manager_name
        self.manager_photo = manager_photo
        self.manager_birthday = manager_birthday
        self.map_src = map_src
        self.logo = logo
        self.sport = sport
        self.league = league
        self.season = season


class Metric(db.Model):
    __tablename__ = "metrics"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    value = db.Column(db.Integer)

    team_id = db.Column(db.Integer, db.ForeignKey("teams.id"))
    team = db.relationship("Team", back_populates="metrics")

    def __init__(self, name, value, team):
        self.name = name
        self.value = value
        self.team = team


class Player(db.Model):
    __tablename__ = "players"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    photo = db.Column(db.String(200))
    weight = db.Column(db.Float)
    age = db.Column(db.Integer)
    height = db.Column(db.Float)
    birthday = db.Column(db.String(30))
    market_currency = db.Column(db.String(10))
    market_value = db.Column(db.Integer)
    rating = db.Column(db.Integer)
    bio = db.Column(db.String(400))
    video_src = db.Column(db.String(200))

    league_id = db.Column(db.Integer, db.ForeignKey("leagues.id"))
    team_id = db.Column(db.Integer, db.ForeignKey("teams.id"))
    sport_id = db.Column(db.Integer, db.ForeignKey("sports.id"))
    team = db.relationship("Team", back_populates="players")
    sport = db.relationship("Sport", back_populates="players")
    league = db.relationship("League", back_populates="players")

    def __init__(
        self,
        id,
        name,
        photo,
        weight,
        age,
        height,
        birthday,
        market_currency,
        market_value,
        rating,
        bio,
        video_src,
        league,
        team,
        sport,
    ):
        self.id = id
        self.name = name
        self.photo = photo
        self.weight = weight
        self.age = age
        self.height = height
        self.birthday = birthday
        self.market_currency = market_currency
        self.market_value = market_value
        self.rating = rating
        self.bio = bio
        self.video_src = video_src
        self.league = league
        self.team = team
        self.sport = sport


with app.app_context():
    db.create_all()
