import requests
import os
from os.path import join, dirname
import json
from retry import retry


# SPORTS_DB API
sports_db_base_url = "https://www.thesportsdb.com/api/v1/json"
# SPORTS_SCORE API
sports_score_base_url = "https://sportscore1.p.rapidapi.com"
sports_score_headers = {
    "X-RapidAPI-Key": os.environ.get("X_RAPID_API_KEY"),
    "X-RapidAPI-Host": os.environ.get("X_RAPID_API_HOST"),
}


# Gets a page of data from a given endpoint
def get_page(page, endpoint):
    url = f"{sports_score_base_url}/{endpoint}"
    params = {"page": page}
    response = requests.request("GET", url, headers=sports_score_headers, params=params)
    return response.json()["data"]


# Gets all data from a given endpoint
def get_all(endpoint):
    data = []
    page = 1
    while True:
        data_page = get_page(page, endpoint)

        if not data_page:
            break
        else:
            data += data_page
            page += 1

    return data


# Fetch Methods


def get_sport(sport_id, count):
    endpoint = f"sports/{sport_id}/leagues"
    data_page = get_page(1, endpoint)
    return data_page[:count] if data_page else []


def get_assorted_leagues():
    leagues = []
    LEAGUES_PER_SPORT = 9
    for sport_id in range(1, 7):
        leagues += get_sport(sport_id, LEAGUES_PER_SPORT)
    return leagues


def get_players_by_team(team_id):
    players = get_all(f"teams/{team_id}/players")
    return players


def get_team_by_season_id(season_id):
    teams = get_all(f"seasons/{season_id}/teams")
    return teams


def get_team_data(team_id):
    data_page = get_page(1, f"teams/{team_id}")
    return data_page


def get_all_sports():
    data_page = get_page(1, "sports")
    return data_page


def get_season_by_league_id(league_id):
    return get_page(1, f"leagues/{league_id}/seasons")


@retry(ConnectionError, delay=2, backoff=2, tries=3)
def get_sports_db_player(playerName):
    curr_name = (playerName).replace(" ", "%20")
    db_url = f"{sports_db_base_url}/3/searchplayers.php?p={curr_name}"
    sports_db_response = requests.request("GET", db_url)

    try:
        response = sports_db_response.json()["player"][0]
    except:
        response = None

    return response


# Get Stadium Google Maps Embedded Src
@retry(ConnectionError, delay=2, backoff=2, tries=3)
def get_map_src(name):
    GOOGLE_API_KEY = os.environ.get("GOOGLE_API_KEY")
    name = name.replace(" ", "+")
    src = f"https://www.google.com/maps/embed/v1/place?key={GOOGLE_API_KEY}&q={name}"
    return src


@retry(ConnectionError, delay=2, backoff=2, tries=3)
def get_yt_video(player_name):
    GOOGLE_API_KEY = os.environ.get("GOOGLE_API_KEY3")
    url = f"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={player_name}&type=video&key={GOOGLE_API_KEY}"
    response = requests.request("GET", url)

    videos = response.json()["items"]

    if videos:
        video_id = videos[0]["id"]["videoId"]
        video_src = f"//www.youtube.com/embed/{video_id}?rel=0"
    else:
        video_src = ""

    return video_src
