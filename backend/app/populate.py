from app import app
import json
from .models import db, Sport, League, Fact, Season, Team, Metric, Player
from .fetch import *


def populate_db():
    with app.app_context():
        # populate_sports()
        # populate_leagues_and_facts()
        # populate_seasons()
        # populate_teams_and_metrics()
        populate_players()


# Add all sports to db
def populate_sports():
    raw_sports = get_all_sports()

    for sport in raw_sports:
        curated_sport = Sport(sport["id"], sport["name"])
        db.session.add(curated_sport)
    db.session.commit()


# add 200 assorted sports leagues and related facts to db
def populate_leagues_and_facts():
    raw_leagues = get_assorted_leagues()

    for league in raw_leagues:
        # Set defaults for uncertain fields
        if league["host"]:
            country = league["host"]["country"]
            map_src = get_map_src(country)
        else:
            country = "No information about host country"
            map_src = ""

        logo = league["logo"] if league["logo"] else "No logo available"
        start_date = (
            league["start_date"] if league["start_date"] else "No start date available"
        )
        end_date = league["end_date"] if league["end_date"] else "No end date available"

        # Get sport from db
        sport = Sport.query.get(league["sport_id"])

        # Add to league db
        curated_league = League(
            league["id"],
            league["name_translations"]["en"],
            country,
            logo,
            start_date,
            end_date,
            map_src,
            sport=sport,
        )
        db.session.add(curated_league)

        # Populates facts table
        for fact in league["facts"]:
            db.session.add(Fact(fact["name"], fact["value"], league=curated_league))

    db.session.commit()


# add season by league id for each league to db
def populate_seasons():
    # Get all leagues from db
    leagues = League.query.all()

    for league in leagues:
        raw_league_seasons = get_season_by_league_id(league.id)

        # Add each season in league to db
        for season in raw_league_seasons:
            start_year = season["year_start"] if season["year_start"] else 0
            end_year = season["year_end"] if season["year_end"] else 0

            curated_season = Season(
                season["id"], season["name"], start_year, end_year, league=league
            )
            db.session.add(curated_season)

    db.session.commit()


# Add teams
def populate_teams_and_metrics():
    seasons = Season.query.all()
    for season in seasons:
        raw_seasonal_teams = get_team_by_season_id(season.id)

        for team in raw_seasonal_teams:
            # Skip team is null or if team already exists in db
            if not team or Team.query.get(team["id"]):
                continue

            # Fetch from Team Data
            team_data = get_team_data(team["id"])

            # Check if venue exists
            try:
                stadium_name = team_data["venue"]["stadium"]["en"]
                stadium_capacity = team_data["venue"]["stadium_capacity"]
                if not stadium_capacity:
                    stadium_capacity = 0
                map_src = get_map_src(stadium_name)
            except:
                stadium_name = "No information about stadium."
                stadium_capacity = 0
                map_src = ""

            # Check if manage exists
            try:
                manager_name = team_data["manager"]["name"]
                manager_photo = team_data["manager"]["photo"]
                manager_birthday = team_data["manager"]["date_birth"]
                if not manager_birthday:
                    manager_birthday = "No information about manager birthday."
                metrics = team_data["manager"]["performance"]
            except:
                manager_name = "No information about manager name."
                manager_photo = "No manager photo."
                manager_birthday = "No information about manager birthday."
                metrics = {}

            league = League.query.get(season.league_id)
            sport = Sport.query.get(league.sport_id)

            curated_team = Team(
                team["id"],
                team["name"],
                stadium_name,
                stadium_capacity,
                team["country"],
                manager_name,
                manager_photo,
                manager_birthday,
                map_src,
                team["logo"],
                sport=sport,
                league=league,
                season=season,
            )

            # Add metrics for team
            if metrics:
                for metric_name in metrics:
                    add_metrics(metric_name, metrics[metric_name], team=curated_team)

            db.session.add(curated_team)
        db.session.commit()


# Given metric details, create metric instance and add to db
def add_metrics(name, value, team):
    curated_metric = Metric(name, value, team)
    db.session.add(curated_metric)


# Populate players on db
def populate_players():
    teams = Team.query.all()

    for team in teams:
        players = get_players_by_team(team.id)
        for player in players:
            # Skip player if already exists in db
            if Player.query.get(player["id"]):
                continue

            league = League.query.get(team.league_id)

            # Get the player info from SportsDB
            sports_db_player = get_sports_db_player(player["name"])
            bio = get_bio(sports_db_player)
            video_src = ""  # get_yt_video(player['name'])

            height = player["height"] if player["height"] else 0
            weight = player["weight"] if player["weight"] else 0
            market_value = player["market_value"] if player["market_value"] else 0
            rating = player["rating"] if player["rating"] else 0

            curated_player = Player(
                player["id"],
                player["name"],
                player["photo"],
                weight,
                player["age"],
                height,
                player["date_birth_at"],
                player["market_currency"],
                market_value,
                rating,
                bio,
                video_src,
                team=team,
                sport=team.sport,
                league=league,
            )

            db.session.add(curated_player)
            db.session.commit()


# Get bio of given player
def get_bio(player):
    if player and player["strDescriptionEN"]:
        bio = player["strDescriptionEN"]
    else:
        bio = ""
    return bio
