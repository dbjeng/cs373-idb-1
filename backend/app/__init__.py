from flask import Flask
from flask_cors import CORS
import sys
import os
import sys
import os
from os.path import join, dirname
from dotenv import load_dotenv

# .env configuration
dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

# Configure app
app = Flask(__name__)
CORS(app)

# Database
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("ATS_DB_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

from app import routes
