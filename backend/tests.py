from app import app
import unittest

# from schema import *
import json


class PythonUnitTests(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        self.client = app.test_client()

    def testGetAllLeagues(self):
        with self.client:
            response = self.client.get("/leagues")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 54)

    def testGetAllTeams(self):
        with self.client:
            response = self.client.get("/teams")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 1372)

    def testGetAllPlayers(self):
        with self.client:
            response = self.client.get("/players")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 4122)

    def testTotalLeagues(self):
        with self.client:
            response = self.client.get("/total_leagues")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 54)

    def testTotalTeams(self):
        with self.client:
            response = self.client.get("/total_teams")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 1372)

    def testTotalPlayers(self):
        with self.client:
            response = self.client.get("/total_players")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 4122)

    def testLeagueIDs(self):
        with self.client:
            response = self.client.get("/leagues/1")
            self.assertEqual(response.status_code, 200)
            expected_data = {
                "country": "Northern Ireland",
                "end_date": "2023-06-30 23:59:00",
                "id": 1,
                "logo": "https://tipsscore.com/resb/league/northern-ireland-premiership.png",
                "map_src": "https://www.google.com/maps/embed/v1/place?key=AIzaSyAtkOsbm4UUWvN4ufauQM8ZZjH0YZTFjy8&q=Northern+Ireland",
                "name": "Premiership",
                "sport_id": 1,
                "start_date": "2022-08-12 18:45:00",
            }
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testTeamIDs(self):
        with self.client:
            response = self.client.get("/teams/25")
            self.assertEqual(response.status_code, 200)
            expected_data = {
                "country_name": "Norway",
                "id": 25,
                "league_id": 943,
                "logo": "https://tipsscore.com/resb/team/ruud-c.png",
                "manager_birthday": "No information about manager b",
                "manager_name": "No information about manager n",
                "manager_photo": "No manager photo.",
                "map_src": "",
                "name": "Ruud C.",
                "sport_id": 2,
                "stadium_capacity": 0,
                "stadium_name": "No information about stadium.",
            }
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testPlayerIDs(self):
        with self.client:
            response = self.client.get("/players/95")
            self.assertEqual(response.status_code, 200)
            expected_data = {
                "age": 23,
                "bio": "Adam Thomas Lewis (born 8 November 1999) is an English professional footballer who plays as a left-back for Newport County on loan from Liverpool.",
                "birthday": "1999-11-08",
                "height": 1.75,
                "id": 95,
                "league_id": 6,
                "market_currency": "EUR",
                "market_value": 0,
                "name": "Adam Lewis",
                "photo": "https://tipsscore.com/resb/player/adam-lewis.png",
                "rating": 53,
                "sport_id": 1,
                "team_id": 4855,
                "video_src": "",
                "weight": 0.0,
            }
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testSportIDs(self):
        with self.client:
            response = self.client.get("/sports/1")
            self.assertEqual(response.status_code, 200)
            expected_data = {
                "id": 1,
                "name": "Football",
            }
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testFactsbyLeagueIDs(self):
        with self.client:
            response = self.client.get("/facts/1")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 8)

    def testSeasonsbyLeagueIDs(self):
        with self.client:
            response = self.client.get("/seasons/1")
            self.assertEqual(response.status_code, 200)
            expected_data = [
                {
                    "end_year": 2021,
                    "id": 6976,
                    "name": "Premiership 20/21",
                    "start_year": 2020,
                },
                {
                    "end_year": 2022,
                    "id": 9279,
                    "name": "Premiership 21/22",
                    "start_year": 2021,
                },
                {
                    "end_year": 2023,
                    "id": 18970,
                    "name": "Premiership 22/23",
                    "start_year": 2022,
                },
            ]
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testMetricsbyTeamIDs(self):
        with self.client:
            response = self.client.get("/metrics/209")
            self.assertEqual(response.status_code, 200)
            expected_data = [
                {"id": 92, "name": "total", "value": 337},
                {"id": 93, "name": "wins", "value": 209},
                {"id": 94, "name": "draws", "value": 54},
                {"id": 95, "name": "losses", "value": 74},
                {"id": 96, "name": "goals_scored", "value": 686},
                {"id": 97, "name": "goals_conceded", "value": 314},
                {"id": 98, "name": "total_points", "value": 681},
            ]
            data = json.loads(response.data)
            self.assertEqual(expected_data, data)

    def testTeamsByLeagueIDs(self):
        with self.client:
            response = self.client.get("/teams/league/1")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 12)

    def testPlayersByTeamIDs(self):
        with self.client:
            response = self.client.get("/players/team/66")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 25)

    def testSearchAll(self):
        with self.client:
            response = self.client.get("/search?query=handball")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data["leagues"]), 9)
            self.assertEqual(len(data["teams"]), 74)
            self.assertEqual(len(data["players"]), 889)

    def testSearchLeagues(self):
        with self.client:
            response = self.client.get("/leagues/search?terms=handball")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 9)

    def testSearchTeams(self):
        with self.client:
            response = self.client.get("/teams/search?terms=handball")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 74)

    def testSearchPlayers(self):
        with self.client:
            response = self.client.get("/players/search?terms=handball")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 889)

    def testFilterLeagues(self):
        with self.client:
            response = self.client.get(
                "/leagues/filter?filter_type=Country&filter_value=wales"
            )
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 3)

    def testFilterTeams(self):
        with self.client:
            response = self.client.get(
                "/teams/filter?filter_type=country&filter_value=zimbabwe"
            )
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 4)

    def testFilterPlayers(self):
        with self.client:
            response = self.client.get(
                "/players/filter?filter_type=video&filter_value=Yes"
            )
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(len(data), 236)

    def testSortLeagues(self):
        with self.client:
            response = self.client.get("/leagues/sort?sort_type=Name")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            expected_data = {
                "country": "Poland",
                "end_date": "2023-05-28 00:00:00",
                "id": 7366,
                "logo": "https://tipsscore.com/resb/league/poland-1-liga.png",
                "map_src": "https://www.google.com/maps/embed/v1/place?key=AIzaSyAtkOsbm4UUWvN4ufauQM8ZZjH0YZTFjy8&q=Poland",
                "name": "1 Liga",
                "sport_id": 3,
                "start_date": "2022-09-24 11:00:00",
            }
            self.assertEqual(data[0], expected_data)

    def testSortTeams(self):
        with self.client:
            response = self.client.get("/teams/sort?sort_type=Name")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            expected_data = {
                "country_name": "USA",
                "id": 90545,
                "league_id": 7591,
                "logo": "https://tipsscore.com/resb/no-league.png",
                "manager_birthday": "No information about manager b",
                "manager_name": "No information about manager n",
                "manager_photo": "No manager photo.",
                "map_src": "https://www.google.com/maps/embed/v1/place?key=AIzaSyAtkOsbm4UUWvN4ufauQM8ZZjH0YZTFjy8&q=Abbotsford+Centre",
                "name": "Abbotsford Canucks",
                "sport_id": 4,
                "stadium_capacity": 0,
                "stadium_name": "Abbotsford Centre",
            }
            self.assertEqual(data[0], expected_data)

    def testSortPlayers(self):
        with self.client:
            response = self.client.get("/players/sort?sort_type=Age")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            expected_data = {
                "age": 16,
                "bio": "",
                "birthday": "2005-11-21",
                "height": 1.7,
                "id": 395177,
                "league_id": 3,
                "market_currency": "EUR",
                "market_value": 0,
                "name": "Ethan Correia Sousa",
                "photo": "https://tipsscore.com/resb/no-photo.png",
                "rating": 0,
                "sport_id": 1,
                "team_id": 222,
                "video_src": "",
                "weight": 0.0,
            }
            self.assertEqual(data[0], expected_data)


if __name__ == "__main__":
    unittest.main()
